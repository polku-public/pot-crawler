# POT Crawler

> Script to crawl Platform of Trust identity networks and AtDataProductLinks.

## Getting Started

Here's what you need to do.

### Prerequisites

Install npm packages.

```
npm install
```

Fill in required application credentials and root UUID of the identity network to script.js

```
const clientSecret = '<client-secret>'
const appAccessToken = '<access-token>'
const accessToken = '<access-token>'
const rootIdentityId = '<uuid>'
```

## Running the crawler

Crawler starts with the following command.

```
npm start
```
