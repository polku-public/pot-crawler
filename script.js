const rp = require('request-promise')
const crypto = require('crypto')
const moment = require('moment')
const _ = require('lodash')

const brokerAPI = 'https://api.oftrust.net/broker/v1/fetch-data-product'
const identityAPI = 'https://api.oftrust.net/identities/v1/'
const clientSecret = ''
const appAccessToken = ''
const accessToken = ''
const rootIdentityId = ''

const async = true
const requestLogging = false
const queueStatusLogging = true

const maxQueues = 10
const maxRequestAttempts = 10

const IFC_GUID = 'IfcGuid'
const IFC_NAME = 'idLocal'

const dataProductMapping = {}
const identityIdToIfcGuid = {}
const identityIdToIfcName = {}
const productCodeMapping = {}
const sourceToParents = {}
const ifcGuidToIdentityId = {}

const sessions = {}
const events = {}

function request(method, url, headers, body) {
    const options = {
        method: method,
        uri: url,
        json: true,
        body: body,
        resolveWithFullResponse: true,
        headers: headers
    }

    if (requestLogging) console.log(moment().format() + ': ' + '--> ' + method + ' ' + url)

    return rp(options).then(result => Promise.resolve(result))
        .catch((error) => {
            console.log(moment().format() + ': ' + '--> ' + method + ' ' + url + ' ' + error.message)
            if (queueStatusLogging) console.log('maxQueues: ' + maxQueues);
            return Promise.reject(error)
        })
}

function queueTask(eventId, queue, task, params) {
    // Generate an unique ID.
    let id = Math.random().toString(26).slice(2)

    // Add a task to the queue.
    events[eventId][queue].push(id)

    // Start waiting.
    let work = setInterval(async function () {
        if (events[eventId][queue][0] === id) {
            // Remove the task from the queue and stop waiting.
            clearInterval(work)

            // Invoke task.
            await task(...params)
            events[eventId][queue].shift()
        }
    }, 100)
}

function selectQueue(eventId, task, params) {
    if (!Object.hasOwnProperty.call(events, eventId)) {
        events[eventId] = []
        for (let i = 0; i < maxQueues; i++) {
            events[eventId][i] = []
        }
    }
    let selection = 0
    // Check smallest queue.
    for (let i = 0; i < maxQueues; i++) {
        if (!events[eventId][i]) events[eventId][i] = []
        if (events[eventId][i].length < events[eventId][selection].length) selection = i
    }
    queueTask(eventId, selection, task, params)
}

const wait = function (eventId) {
    return new Promise(resolve => {
        let wait = setInterval(async function () {
            let status = []
            for (let i = 0; i < events[eventId].length; i++) {
                status[i] = events[eventId][i].length
            }
            if (queueStatusLogging) console.log(...status)
            if (status.reduce((a, b) => a + b, 0) === 0) {
                // Remove the task from the queue and stop waiting.
                clearInterval(wait)
                resolve()
            }
        }, 100)
    })
}

const getIdentity = async (id) => {
    let res
    for (let i = 0; i < maxRequestAttempts; i++) {
        try {
            if (!res) res = await request('GET', identityAPI + id, {'Authorization': 'Bearer ' + accessToken})
        } catch (e) {
            // console.log(e.message)
            console.log('Retry #' + (i + 1))
        }
    }
    delete res.body.pagination
    delete res.body.metadata
    if (!res) return Promise.reject(new Error('getIdentity failed.'))
    return res
}

const getProductCode = async (id) => {
    let productCode
    const res = await getIdentity(id)
    for (let r = 0; r < res.body.outLinks.length; r++) {
        if (res.body.outLinks[r]['@type'] === 'PublishedOn') {
            productCode = res.body.outLinks[r].data.productCode
        }
    }
    return productCode
}

const saveExternalId = function (sessionId, identity, id) {
    // Fill parameters into an object used in a broker call.
    if (Object.hasOwnProperty.call(identity, 'productCode')) {
        if (!Object.hasOwnProperty.call(identity.data, 'id')) identity.data.id = id
        if (identity.productCode) {
            if (!Object.hasOwnProperty.call(sessions, sessionId)) sessions[sessionId] = {dataProducts: {}}
            if (!Object.hasOwnProperty.call(sessions[sessionId].dataProducts, identity.productCode)) {
                sessions[sessionId].dataProducts[identity.productCode] = [identity.data]
            } else {
                sessions[sessionId].dataProducts[identity.productCode].push(identity.data)
            }
        }
    }
}

const checkDataProductLinks = async (sessionId, identity, parent) => {
    if (!parent) parent = {}
    for (let o = 0; o < identity.outLinks.length; o++) {
        if (identity.outLinks[o]['@type'] === 'AtDataProduct' && parent) {
            // Get product code for the data product.
            // Check for existing mapping.
            if (Object.hasOwnProperty.call(dataProductMapping, identity.outLinks[o].to)) {
                // Use existing mapping.
                identity.outLinks[o].productCode = dataProductMapping[identity.outLinks[o].to]
            } else {
                // Save received product code.
                const productCode = await getProductCode(identity.outLinks[o].to)
                if (productCode) {
                    identity.outLinks[o].productCode = productCode
                    dataProductMapping[identity.outLinks[o].to] = identity.outLinks[o].productCode
                } else {
                    continue
                }
            }

            if (identity['@type'] === 'Room') {
                parent = JSON.parse(JSON.stringify(identity))
            }

            // Current identity, which has an identity with AtDataProduct belongsTo a parent identity.
            // This parent is used for data visualization (parent: space -> child: sensor).
            if (!parent.data) {
                // console.log('--> No data object with parent ' + parent['@type'] + ', ' + parent['@id'])
            } else {
                // console.log('--> Parent data object: ' + JSON.stringify(parent.data))
            }

            // Check external id for parent if productCode exists.
            if (Object.hasOwnProperty.call(parent, 'data') && Object.hasOwnProperty.call(identity.outLinks[o], 'productCode')) {
                if (Object.hasOwnProperty.call(parent.data, IFC_GUID)) {
                    // Save externalIdMapping
                    ifcGuidToIdentityId[parent.data[IFC_GUID]] = parent['@id']
                    identityIdToIfcGuid[parent['@id']] = parent.data[IFC_GUID]
                    identityIdToIfcName[parent['@id']] = parent.data[IFC_NAME]

                    // Store mapping for child and parent for relationships containing AtDataProduct links.
                    if (!Object.hasOwnProperty.call(sourceToParents, identity['@id'])) {
                        sourceToParents[identity['@id']] = [parent['@id']]
                    } else {
                        if (!sourceToParents[identity['@id']].includes(parent['@id'])) {
                            sourceToParents[identity['@id']].push(parent['@id'])
                        }
                    }

                    // Save mapping for product code and external id.
                    if (Object.hasOwnProperty.call(identity.outLinks[o], 'data')) {
                        if (Object.hasOwnProperty.call(identity.outLinks[o].data, 'id')) {
                            if (!Object.hasOwnProperty.call(productCodeMapping, identity.outLinks[o].productCode)) productCodeMapping[identity.outLinks[o].productCode] = {}
                            productCodeMapping[identity.outLinks[o].productCode][JSON.stringify(identity.outLinks[o].data.id)] = identity['@id']
                        } else {
                            if (!Object.hasOwnProperty.call(productCodeMapping, identity['outLinks'][o].productCode)) productCodeMapping[identity['outLinks'][o].productCode] = {}
                            productCodeMapping[identity['outLinks'][o].productCode][JSON.stringify(identity['@id'])] = identity['@id']
                        }
                    }
                }
            }

            // Store external id.
            if (Object.hasOwnProperty.call(identity.outLinks[o], 'productCode')) saveExternalId(sessionId, identity.outLinks[o], identity['@id'])
        }
    }
    return identity
}

const getChildren = async (sessionId, identity, path, direction, parent) => {
    await checkDataProductLinks(sessionId, identity, parent)
    if (Object.hasOwnProperty.call(identity, direction)) {
        for (let i = 0; i < identity[direction].length; i++) {
            const res = await getIdentity(identity[direction][i].from)
            const childPath = path + '.children.' + i

            // Async
            if (async) selectQueue(sessionId, getChildren, [sessionId, res.body, childPath, direction])

            // Synchronous
            else await getChildren(sessionId, res.body, childPath, direction, identity)
        }
    }
}

const getData = async (sessionId, startTime, endTime, sourceId, query) => {
    const data = {}
    const reqIds = {}
    let convertedId
    if (!Object.hasOwnProperty.call(sessions, sessionId)) return data
    for (let i = 0; i < Object.keys(sessions[sessionId].dataProducts).length; i++) {
        try {
            const productCode = Object.keys(sessions[sessionId].dataProducts)[i]
            if (sourceId) {
                // convert sourceId to externalId.
                if (Object.hasOwnProperty.call(productCodeMapping, productCode)) {
                    if (Object.values(productCodeMapping[productCode]).includes(sourceId)) {
                        convertedId = Object.entries(productCodeMapping[productCode]).find(o => o[1] === sourceId)
                        if (convertedId) convertedId = convertedId[0]
                    }
                }
            }
            // Combine ids from distinct product codes.
            let ids = []
            for (let a = 0; a < Object.values(sessions[sessionId].dataProducts)[i].length; a++) {
                ids.push(Object.values(sessions[sessionId].dataProducts)[i][a].id)
            }

            if (sourceId) {
                if (!convertedId) continue
                if (!ids.map(o => JSON.stringify(o)).includes(convertedId)) continue
                ids = [JSON.parse(convertedId)]
            }

            // Get data.
            const body = {
                timestamp: moment().format(),
                productCode,
                parameters: {}
            }
            if (endTime) body.parameters.endTime = moment(parseInt(endTime)).format()
            if (productCode === 'user-feedback-1') {
                if (!endTime && !startTime) {
                    body.parameters.endTime = moment().format()
                    if (query) {
                        if (query.end) {
                            body.parameters.endTime = moment(parseInt(query.end)).format()
                        }
                    }
                }
            }
            body.parameters.ids = _.uniq(ids)
            if (startTime) body.parameters.startTime = moment(parseInt(startTime)).format()
            if (productCode === 'user-feedback-1') {
                if (!endTime && !startTime) {
                    body.parameters.startTime = moment(moment() - 1000 * 60 * 60 * 24 * 7 * 2).format()
                    if (query) {
                        if (query.start) {
                            body.parameters.startTime = moment(parseInt(query.start)).format()
                        }
                    }
                }
            }
            let res
            for (let i = 0; i < maxRequestAttempts; i++) {
                try {
                    if (!res) res = await fetchDataProduct(body)
                } catch (e) {
                    // console.log(e.message)
                    console.log('Retry #' + (i + 1))
                }
            }
            if (res) {
                reqIds[Object.keys(sessions[sessionId].dataProducts)[i]] = _.uniq(ids)
                data[Object.keys(sessions[sessionId].dataProducts)[i]] = res.body.data.items
            }
        } catch (e) {
            console.log(e.message)
        }
    }
    return {ids: reqIds, data}
}

const convertDataObject = function (array) {
    return array
}

const compileIndexedSources = async (data, history) => {
    const identityIds = Object.keys(data)
    const indexedData = {}
    for (let i = 0; i < Object.values(data).length; i++) {
        if (Object.hasOwnProperty.call(sourceToParents, identityIds[i])) {
            for (let p = 0; p < sourceToParents[identityIds[i]].length; p++) {
                // Index sources based on parent id.
                if (!Object.hasOwnProperty.call(indexedData, sourceToParents[identityIds[i]][p])) indexedData[sourceToParents[identityIds[i]][p]] = []

                // Create a source under parent.
                const res = await getIdentity(identityIds[i])
                if (res) {
                    const sources = {}
                    // Loop through different timestamps.
                    for (let d = 0; d < Object.values(data)[i].data.length; d++) {
                        let found = null
                        if (!Object.hasOwnProperty.call(sources, Object.values(data)[i].data[d].timestamp)) {
                            sources[Object.values(data)[i].data[d].timestamp] = []
                        } else {
                            for (let c = 0; c < sources[Object.values(data)[i].data[d].timestamp].length; c++) {
                                if (sources[Object.values(data)[i].data[d].timestamp][c].source_name.includes(Object.values(data)[i].id)) {
                                    found = c
                                }
                            }
                        }
                        const source = JSON.parse(JSON.stringify(res.body))
                        source.source_name = source['@type'] + ' ' + Object.values(data)[i].id
                        source.source_id = source['@id']
                        delete source['@context']
                        delete source['@type']
                        delete source['@id']
                        delete source.inLinks
                        delete source.outLinks
                        delete source.metadata
                        source.timestamp = Object.values(data)[i].data[d].timestamp
                        source.data = convertDataObject([Object.values(data)[i].data[d]])
                        if (found === null) {
                            sources[Object.values(data)[i].data[d].timestamp].push(source)
                        } else {
                            sources[Object.values(data)[i].data[d].timestamp][found].data.push(...convertDataObject([Object.values(data)[i].data[d]]))
                        }
                    }
                    let objectsBySourceId
                    try {
                        // Combine data from same source id.
                        const objs = Object.values(sources).reduce(function (a, b) {
                            return a.concat(b)
                        }, [])
                        const sourceIds = _.uniq(objs.map(y => y.source_id))
                        if (!history) {
                            objectsBySourceId = sourceIds.map((id) => {
                                // Find latest timestamp => source.timestamp.
                                // Combine data arrays (sort based on timestamp?).
                                // Find data by source id.
                                const entry = {}
                                entry.source_id = id
                                const sourcesWithSameId = objs.filter(d => d.source_id === id).sort((a, b) => b.timestamp - a.timestamp).map((s) => {
                                    for (let i = 0; i < s.data.length; i++) {
                                        s.data[i].timestamp = s.timestamp
                                    }
                                    return s
                                })
                                entry.data = sourcesWithSameId.map(s => s.data).reduce((a, b) => a.concat(b), []).sort((a, b) => b.timestamp - a.timestamp)
                                entry.source_name = sourcesWithSameId.length > 0 ? sourcesWithSameId[0].source_name : ''
                                entry.timestamp = sourcesWithSameId.length > 0 ? sourcesWithSameId[0].timestamp : 0
                                return entry
                            })
                        }
                    } catch (e) {
                        console.log(e.message)
                    }

                    indexedData[sourceToParents[identityIds[i]][p]] = indexedData[sourceToParents[identityIds[i]][p]].concat(...objectsBySourceId || Object.values(sources))
                }
            }
        }
    }
    return indexedData
}

const getPOTData = function (start, end, sourceId, query) {
    const history = start && end
    const sessionId = (new Date().getTime()) + Math.random().toString(26).slice(2) + ''
    return getIdentity(rootIdentityId).then((res) => {
        // inLinks or outLinks.
        return getChildren(sessionId, res.body, 0, 'inLinks')
    }).then(res => {
        return async ? wait(sessionId) : Promise.resolve()
    }).then((res) => {
        return getData(sessionId, start, end, sourceId, query)
    }).then((res) => {
        delete sessions[sessionId]
        const productCodes = Object.keys(res.data)
        const data = {}

        // Index data based on source identity id.
        for (let i = 0; i < Object.values(res.data).length; i++) {
            for (let h = 0; h < Object.values(res.data)[i].length; h++) {
                // Check for product code mapping.
                if (Object.hasOwnProperty.call(productCodeMapping, productCodes[i])) {
                    // Check for identity id mapping.
                    if (Object.hasOwnProperty.call(productCodeMapping[productCodes[i]], JSON.stringify(Object.values(res.data)[i][h].id))) {
                        const identityId = productCodeMapping[productCodes[i]][JSON.stringify(Object.values(res.data)[i][h].id)]
                        // Format array for received data.
                        if (!Object.hasOwnProperty.call(data, identityId)) data[productCodeMapping[productCodes[i]][JSON.stringify(Object.values(res.data)[i][h].id)]] = {data: []}
                        // Store external id to received data objects.
                        data[identityId].id = JSON.stringify(Object.values(res.data)[i][h].id)
                        data[identityId].data = data[identityId].data.concat(Object.values(res.data)[i][h].data).map((o) => {
                            // Convert timestamps to millis.
                            o.timestamp = (new Date(o.timestamp)).getTime()
                            return o
                        })
                        // Check received data.
                        if (Object.hasOwnProperty.call(res.ids, productCodes[i])) {
                            if (data[identityId].data.length > 0) {
                                res.ids[productCodes[i]] = res.ids[productCodes[i]].filter(id => JSON.stringify(id) !== data[identityId].id)
                                if (res.ids[productCodes[i]].length === 0) {
                                    delete res.ids[productCodes[i]]
                                }
                            }
                        }
                    }
                }
            }
        }
        return compileIndexedSources(data, history)
    }).then((res) => {
        const identityIds = Object.keys(res)
        const indexedByIfcName = {}

        // Index data based on IFC Guids.
        for (let i = 0; i < Object.values(res).length; i++) {
            indexedByIfcName[identityIdToIfcName[identityIds[i]]] = Object.values(res)[i]
        }
        if (sourceId) return [].concat.apply([], Object.values(indexedByIfcName))
        return indexedByIfcName
    }).catch((err) => {
        delete sessions[sessionId]
        return {}
    })
}

const fetchDataProduct = async (object) => {
    // Sort request body.
    const body = {}
    Object.keys(object).sort().forEach((k) => {
        body[k] = object[k]
    })

    if (Object.hasOwnProperty.call(body, 'parameters')) {
        const sortedParameters = {}
        Object.keys(object.parameters).sort().forEach((k) => {
            sortedParameters[k] = object.parameters[k]
        })
        body.parameters = sortedParameters
    }

    // Create HMAC-SHA256 signature in base64 encoded format.
    const signature = crypto.createHmac('sha256', Buffer.from(clientSecret, 'utf8'))
        .update(JSON.stringify(body)
            .replace(/[\u007F-\uFFFF]/g, chr => '\\u' + ('0000' + chr.charCodeAt(0).toString(16)).substr(-4))
            .replace(new RegExp('":', 'g'), '": '))
        .digest('base64')

    // Set request headers
    const headers = {
        'X-Pot-Signature': signature,
        'X-App-Token': appAccessToken,
        'X-User-Token': accessToken,
        'Content-Type': 'application/json'
    }

    return request('POST', brokerAPI, headers, body)
}

const userId = JSON.parse(Buffer.from(accessToken.split('.')[1], 'base64').toString('binary')).sub
console.log('Using user/application with ID: ' + userId)

getPOTData().then(res => {
    console.log(res)
}).catch(err => {
    console.log(err.message)
})

const stressTest = function () {
    const eventId1 = (new Date().getTime()) + Math.random().toString(26).slice(2) + ''

    // Start from 1.
    maxQueues = 1;

    // Send requests
    setInterval(function () {
        selectQueue(eventId1, request, ['GET', identityAPI + rootIdentityId, { 'Authorization': 'Bearer ' + accessToken }])
    }, 5)

    // Log queue status.
    setInterval(function () {
        if (Object.hasOwnProperty.call(events, eventId1)) {
            let status = []
            for (let i = 0; i < events[eventId1].length; i++) {
                status[i] = events[eventId1][i].length
            }
            if (queueStatusLogging) console.log(...status)
        }
    }, 100)

    // Increase the amount of queues.
    setInterval(function () {
        maxQueues++
    }, 100)
}

// stressTest();
